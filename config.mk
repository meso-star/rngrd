VERSION = 0.1.0
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

MRUMTL_VERSION = 0.2
MRUMTL_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags mrumtl)
MRUMTL_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs mrumtl)

RNSL_VERSION = 0.1
RNSL_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rnsl)
RNSL_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rnsl)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

S3D_VERSION = 0.10
S3D_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags s3d)
S3D_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs s3d)

SBUF_VERSION = 0.1
SBUF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags sbuf)
SBUF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs sbuf)

SMSH_VERSION = 0.1
SMSH_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags smsh)
SMSH_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs smsh)

SSF_VERSION = 0.9
SSF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags ssf)
SSF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs ssf)

DPDC_CFLAGS =\
 $(MRUMTL_CFLAGS)\
 $(RNSL_CFLAGS)\
 $(RSYS_CFLAGS)\
 $(S3D_CFLAGS)\
 $(SBUF_CFLAGS)\
 $(SMSH_CFLAGS)\
 $(SSF_CFLAGS)

DPDC_LIBS =\
 $(MRUMTL_LIBS)\
 $(RNSL_LIBS)\
 $(RSYS_LIBS)\
 $(S3D_LIBS)\
 $(SBUF_LIBS)\
 $(SMSH_LIBS)\
 $(SSF_LIBS)\
 -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

CFLAGS_SO = $(CFLAGS) -fPIC
CFLAGS_EXE = $(CFLAGS) -fPIE

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
