# Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
# Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
# Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
# Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2022, 2023 Observatoire de Paris
# Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
# Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
# Copyright (C) 2022, 2023 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = librngrd.a
LIBNAME_SHARED = librngrd.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/rngrd.c src/rngrd_log.c src/rngrd_mesh.c src/rngrd_properties.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): librngrd.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

librngrd.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(MRUMTL_VERSION) mrumtl; then \
	  echo "mrumtl $(MRUMTL_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RNSL_VERSION) rnsl; then \
	  echo "rnsl $(RNSL_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(S3D_VERSION) s3d; then \
	  echo "s3d $(S3D_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SBUF_VERSION) sbuf; then \
	  echo "sbuf $(SBUF_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SMSH_VERSION) smsh; then \
	  echo "smsh $(SMSH_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSF_VERSION) ssf; then \
	  echo "ssf $(SSF_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DRNGRD_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@MRUMTL_VERSION@#$(MRUMTL_VERSION)#g'\
	    -e 's#@RNSL_VERSION@#$(RNSL_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SBUF_VERSION@#$(SBUF_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SSF_VERSION@#$(SSF_VERSION)#g'\
	    rngrd.pc.in > rngrd.pc

rngrd-local.pc: rngrd.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@MRUMTL_VERSION@#$(MRUMTL_VERSION)#g'\
	    -e 's#@RNSL_VERSION@#$(RNSL_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@S3D_VERSION@#$(S3D_VERSION)#g'\
	    -e 's#@SBUF_VERSION@#$(SBUF_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SSF_VERSION@#$(SSF_VERSION)#g'\
	    rngrd.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" rngrd.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/rad-net" src/rngrd.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/rngrd" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" rnsp.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/rngrd.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rngrd/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rngrd/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/rad-net/rngrd.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/rnsp.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(LIBNAME)
	rm -f .config librngrd.o rngrd.pc rngrd-local.pc

distclean: clean
	rm -f $(DEP) src/test_rngrd.d

lint:
	shellcheck -o all make.sh
	mandoc -Tlint -Wall rnsp.5 || [ $$? -le 1 ]

################################################################################
# Tests
################################################################################
TEST_SRC = src/test_rngrd.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
RNGRD_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rngrd-local.pc)
RNGRD_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rngrd-local.pc)

build_tests: build_library src/test_rngrd.d
	@$(MAKE) -fMakefile -f src/test_rngrd.d test_rngrd

clean_test:
	rm -f test_rngrd src/test_rngrd.o

$(TEST_DEP): config.mk rngrd-local.pc
	@$(CC) $(CFLAGS_EXE) $(RNGRD_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_rngrd.o: config.mk rngrd-local.pc
	$(CC) $(CFLAGS_EXE) $(RNGRD_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_rngrd: src/test_rngrd.o config.mk rngrd-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RNGRD_LIBS) $(RSYS_LIBS)
