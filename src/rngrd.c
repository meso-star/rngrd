/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rngrd.h"
#include "rngrd_c.h"
#include "rngrd_log.h"

#include <mrumtl.h>

#include <star/s3d.h>
#include <star/sbuf.h>
#include <star/ssf.h>

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>
#include <rsys/str.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_rngrd_create_args(const struct rngrd_create_args* args)
{
  /* Invalid args */
  if(!args) {
    return RES_BAD_ARG;
  }
  /* Filenames cannot be NULL */
  if(!args->smsh_filename
  || !args->props_filename
  || !args->mtllst_filename) {
    return RES_BAD_ARG;
  }
  /* The name cannot be NULL */
  if(!args->name) {
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
create_rngrd
  (const struct rngrd_create_args* args,
   struct rngrd** out_ground)
{
  struct rngrd* ground = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_ground) { res = RES_BAD_ARG; goto error;}
  res = check_rngrd_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  ground = MEM_CALLOC(allocator, 1, sizeof(*ground));
  if(!ground) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the device of the Rad-Net GRounD library"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&ground->ref);
  ground->allocator = allocator;
  ground->verbose = args->verbose;
  darray_mtl_init(ground->allocator, &ground->mtls);
  str_init(ground->allocator, &ground->name);
  if(args->logger) {
    ground->logger = args->logger;
  } else {
    res = setup_log_default(ground);
    if(res != RES_OK) {
      if(args->verbose) {
        fprintf(stderr, MSG_ERROR_PREFIX
          "Could not setup the default logger of the Rad-Net GRounD library.\n");
      }
      goto error;
    }
  }

  res = str_set(&ground->name, args->name);
  if(res != RES_OK) {
    log_err(ground, "Could not setup the ground name to `%s' -- %s\n",
      args->name, res_to_cstr(res));
    goto error;
  }

exit:
  if(out_ground) *out_ground = ground;
  return res;
error:
  if(ground) { RNGRD(ref_put(ground)); ground = NULL; }
  goto exit;
}

static void
release_rngrd(ref_T* ref)
{
  struct rngrd* ground = CONTAINER_OF(ref, struct rngrd, ref);
  ASSERT(ref);

  if(ground->logger == &ground->logger__) logger_release(&ground->logger__);
  if(ground->s3d) S3D(device_ref_put(ground->s3d));
  if(ground->s3d_view) S3D(scene_view_ref_put(ground->s3d_view));
  if(ground->props) SBUF(ref_put(ground->props));
  darray_mtl_release(&ground->mtls);
  str_release(&ground->name);
  MEM_RM(ground->allocator, ground);
}

/*******************************************************************************
 * Exported symbols
 ******************************************************************************/
res_T
rngrd_create
  (const struct rngrd_create_args* args,
   struct rngrd** out_ground)
{
  struct rngrd* ground = NULL;
  res_T res = RES_OK;

  res = create_rngrd(args, &ground);
  if(res != RES_OK) goto error;

  res = setup_mesh(ground, args);
  if(res != RES_OK) goto error;
  res = setup_properties(ground, args);
  if(res != RES_OK) goto error;

exit:
  if(out_ground) *out_ground = ground;
  return res;
error:
  if(ground) { RNGRD(ref_put(ground)); ground = NULL; }
  goto exit;
}

res_T
rngrd_ref_get(struct rngrd* ground)
{
  if(!ground) return RES_BAD_ARG;
  ref_get(&ground->ref);
  return RES_OK;
}

res_T
rngrd_ref_put(struct rngrd* ground)
{
  if(!ground) return RES_BAD_ARG;
  ref_put(&ground->ref, release_rngrd);
  return RES_OK;
}

res_T
rngrd_validate(const struct rngrd* ground)
{
  res_T res = RES_OK;

  if(!ground) { res = RES_BAD_ARG; goto error; }

  res = check_properties(ground);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
bsdf_init(struct mem_allocator* allocator, struct ssf_bsdf** bsdf)
{
  ASSERT(bsdf);
  (void)allocator;
  *bsdf = NULL;
  return RES_OK;
}

void
bsdf_release(struct ssf_bsdf** bsdf)
{
  ASSERT(bsdf);
  if(*bsdf) SSF(bsdf_ref_put(*bsdf));
}

res_T
bsdf_copy(struct ssf_bsdf** dst, struct ssf_bsdf* const* src)
{
  ASSERT(dst && src);
  if(*dst) SSF(bsdf_ref_put(*dst));
  *dst = *src;
  if(*dst) SSF(bsdf_ref_get(*dst));
  return RES_OK;
}

res_T
bsdf_copy_and_release(struct ssf_bsdf** dst, struct ssf_bsdf** src)
{
  ASSERT(dst && src);
  if(*dst) SSF(bsdf_ref_put(*dst));
  *dst = *src;
  *src = NULL;
  return RES_OK;
}


res_T
mtl_init(struct mem_allocator* allocator, struct mtl* mtl)
{
  ASSERT(mtl);
  mtl->mrumtl = NULL;
  darray_bsdf_init(allocator, &mtl->bsdf_lst);
  return RES_OK;
}

void
mtl_release(struct mtl* mtl)
{
  ASSERT(mtl);
  if(mtl->mrumtl) MRUMTL(ref_put(mtl->mrumtl));
  darray_bsdf_release(&mtl->bsdf_lst);
}

res_T
mtl_copy(struct mtl* dst, const struct mtl* src)
{
  ASSERT(dst && src);
  if(dst->mrumtl) MRUMTL(ref_put(dst->mrumtl));
  dst->mrumtl = src->mrumtl;
  if(dst->mrumtl) MRUMTL(ref_get(dst->mrumtl));
  return darray_bsdf_copy(&dst->bsdf_lst, &src->bsdf_lst);;
}

res_T
mtl_copy_and_release(struct mtl* dst, struct mtl* src)
{
  ASSERT(dst && src);
  if(dst->mrumtl) MRUMTL(ref_put(dst->mrumtl));
  dst->mrumtl = src->mrumtl;
  src->mrumtl = NULL;
  return darray_bsdf_copy_and_release(&dst->bsdf_lst, &src->bsdf_lst);
}
