/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNGRD_C_H
#define RNGRD_C_H

#include <rsys/dynamic_array.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

struct rngrd_create_args;
struct mrumtl;
struct ssf_bsdf;

/*******************************************************************************
 * BSDF
 ******************************************************************************/
extern LOCAL_SYM res_T
bsdf_init
  (struct mem_allocator* allocator,
   struct ssf_bsdf** bsdf);

extern LOCAL_SYM void
bsdf_release
  (struct ssf_bsdf** bsdf);

extern LOCAL_SYM res_T
bsdf_copy
  (struct ssf_bsdf** dst,
   struct ssf_bsdf* const* src);

extern LOCAL_SYM res_T
bsdf_copy_and_release
  (struct ssf_bsdf** dst,
   struct ssf_bsdf** src);

/* Generate the dynamic array of BSDFs */
#define DARRAY_NAME bsdf
#define DARRAY_DATA struct ssf_bsdf*
#define DARRAY_FUNCTOR_INIT bsdf_init
#define DARRAY_FUNCTOR_RELEASE bsdf_release
#define DARRAY_FUNCTOR_COPY bsdf_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE bsdf_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Material
 ******************************************************************************/
struct mtl {
  struct mrumtl* mrumtl;
  struct darray_bsdf bsdf_lst;
};

extern LOCAL_SYM res_T
mtl_init
  (struct mem_allocator* allocator,
   struct mtl* mtl);

extern LOCAL_SYM void
mtl_release
  (struct mtl* mtl);

extern LOCAL_SYM res_T
mtl_copy
  (struct mtl* dst,
   const struct mtl* src);

extern LOCAL_SYM res_T
mtl_copy_and_release
  (struct mtl* dst,
   struct mtl* src);

/* Generate the dynamic array of materials */
#define DARRAY_NAME mtl
#define DARRAY_DATA struct mtl
#define DARRAY_FUNCTOR_INIT mtl_init
#define DARRAY_FUNCTOR_RELEASE mtl_release
#define DARRAY_FUNCTOR_COPY mtl_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE mtl_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Ground
 ******************************************************************************/
struct rngrd {
  /* Geometry */
  struct s3d_device* s3d;
  struct s3d_scene_view* s3d_view;
  size_t ntriangles;

  /* Properties */
  struct darray_mtl mtls;
  struct sbuf* props;

  struct str name;

  int verbose;
  struct logger* logger;
  struct logger logger__;
  struct mem_allocator* allocator;
  ref_T ref;
};

extern LOCAL_SYM res_T
setup_mesh
  (struct rngrd* ground,
   const struct rngrd_create_args* args);

extern LOCAL_SYM res_T
setup_properties
  (struct rngrd* ground,
   const struct rngrd_create_args* args);

extern LOCAL_SYM res_T
check_properties
  (const struct rngrd* ground);

#endif /* RNGRD_C_H */
