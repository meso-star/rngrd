/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rngrd.h"
#include "rngrd_c.h"
#include "rngrd_log.h"

#include <mrumtl.h>
#include <rad-net/rnsl.h>

#include <star/sbuf.h>
#include <star/ssf.h>

#include <rsys/cstr.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_create_bsdf_args
  (const struct rngrd* ground,
   const struct rngrd_create_bsdf_args* args)
{
  double sum_bcoords;
  ASSERT(ground);

  if(!args) return RES_BAD_ARG;

  /* Invalid primitive */
  if(args->prim.prim_id >= ground->ntriangles)
    return RES_BAD_ARG;

  /* Invalid barycentric_coords */
  sum_bcoords =
    args->barycentric_coords[0]
  + args->barycentric_coords[1]
  + args->barycentric_coords[2];
  if(!eq_eps(sum_bcoords, 1, 1.e-6))
    return RES_BAD_ARG;

  /* Invalid random number */
  if(args->r < 0 || args->r >= 1)
    return RES_BAD_ARG;

  /* Invalid wavelength */
  if(args->wavelength < 0)
    return RES_BAD_ARG;

  return RES_OK;
}

static INLINE res_T
check_get_temperature_args
  (const struct rngrd* ground,
   const struct rngrd_get_temperature_args* args)
{
  double sum_bcoords;
  ASSERT(ground);

  if(!args) return RES_BAD_ARG;

  /* Invalid primitive */
  if(args->prim.prim_id >= ground->ntriangles)
    return RES_BAD_ARG;

  /* Invalid barycentric_coords */
  sum_bcoords =
    args->barycentric_coords[0]
  + args->barycentric_coords[1]
  + args->barycentric_coords[2];
  if(!eq_eps(sum_bcoords, 1, 1.e-6))
    return RES_BAD_ARG;

  return RES_OK;
}

static res_T
check_sbuf_desc
  (const struct rngrd* ground,
   const struct sbuf_desc* desc,
   const struct rngrd_create_args* args)
{
  ASSERT(ground && desc && args);

  if(desc->size != ground->ntriangles) {
    log_err(ground,
      "%s: no sufficient surface properties regarding the mesh %s\n",
      args->props_filename, args->smsh_filename);
    return RES_BAD_ARG;
  }

  if(desc->szitem != 8 || desc->alitem != 8 || desc->pitch != 8) {
    log_err(ground, "%s: unexpected layout of properties\n",
      args->props_filename);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
create_bsdf_diffuse
  (struct rngrd* ground,
   const struct mrumtl_brdf_lambertian* lambertian,
   struct ssf_bsdf** out_bsdf)
{
  struct ssf_bsdf* bsdf = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;
  ASSERT(ground && lambertian && out_bsdf);

  allocator = ground->allocator;

  res = ssf_bsdf_create(allocator, &ssf_lambertian_reflection, &bsdf);
  if(res != RES_OK) goto error;
  res = ssf_lambertian_reflection_setup(bsdf, lambertian->reflectivity);
  if(res != RES_OK) goto error;

exit:
  *out_bsdf = bsdf;
  return res;
error:
   if(bsdf) { SSF(bsdf_ref_put(bsdf)); bsdf = NULL; }
  goto exit;
}

static res_T
create_bsdf_specular
  (struct rngrd* ground,
   const struct mrumtl_brdf_specular* specular,
   struct ssf_bsdf** out_bsdf)
{
  struct ssf_bsdf* bsdf = NULL;
  struct ssf_fresnel* fresnel = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;
  ASSERT(ground && specular && out_bsdf);

  allocator = ground->allocator;

  res = ssf_bsdf_create(allocator, &ssf_specular_reflection, &bsdf);
  if(res != RES_OK) goto error;
  res = ssf_fresnel_create(allocator, &ssf_fresnel_constant, &fresnel);
  if(res != RES_OK) goto error;
  res =  ssf_fresnel_constant_setup(fresnel, specular->reflectivity);
  if(res != RES_OK) goto error;
  res = ssf_specular_reflection_setup(bsdf, fresnel);
  if(res != RES_OK) goto error;

exit:
  if(fresnel) SSF(fresnel_ref_put(fresnel));
  *out_bsdf = bsdf;
  return res;
error:
  if(bsdf) { SSF(bsdf_ref_put(bsdf)); bsdf = NULL; }
  goto exit;
}

static INLINE res_T
create_bsdf
  (struct rngrd* ground,
   const struct mrumtl_brdf* brdf,
   struct ssf_bsdf** out_bsdf)
{
  struct mrumtl_brdf_lambertian lambertian;
  struct mrumtl_brdf_specular specular;
  struct ssf_bsdf* bsdf = NULL;
  res_T res = RES_OK;
  ASSERT(ground && brdf && out_bsdf);

  switch(mrumtl_brdf_get_type(brdf)) {
    case MRUMTL_BRDF_LAMBERTIAN:
      MRUMTL(brdf_get_lambertian(brdf, &lambertian));
      res = create_bsdf_diffuse(ground, &lambertian, &bsdf);
      if(res != RES_OK) goto error;
      break;
    case MRUMTL_BRDF_SPECULAR:
      MRUMTL(brdf_get_specular(brdf, &specular));
      res = create_bsdf_specular(ground, &specular, &bsdf);
      if(res != RES_OK) goto error;
      break;
    default: FATAL("Unreachable code.\n");  break;
  }

exit:
  *out_bsdf = bsdf;
  return res;
error:
  if(bsdf) { SSF(bsdf_ref_put(bsdf)); bsdf = NULL; }
  goto exit;
}

static res_T
mtl_create_bsdf_list
  (struct rngrd* ground,
   const char* filename,
   struct mtl* mtl)
{
  size_t nbsdfs = 0;
  size_t ibsdf = 0;
  res_T res = RES_OK;
  ASSERT(ground && filename && mtl);

  /* Reserve memory space for the list of BSDFs */
  nbsdfs = mrumtl_get_brdfs_count(mtl->mrumtl);
  res = darray_bsdf_resize(&mtl->bsdf_lst, nbsdfs);
  if(res != RES_OK) goto error;

  FOR_EACH(ibsdf, 0, nbsdfs) {
    const struct mrumtl_brdf* brdf = mrumtl_get_brdf(mtl->mrumtl, ibsdf);
    struct ssf_bsdf** bsdf = darray_bsdf_data_get(&mtl->bsdf_lst) + ibsdf;

    res = create_bsdf(ground, brdf, bsdf);
    if(res != RES_OK) goto error;
  }

exit:
  return res;

error:
  log_err(ground, "%s: error creating the list of BSDFs -- %s\n",
    filename, res_to_cstr(res));
  darray_bsdf_clear(&mtl->bsdf_lst);
  goto exit;
}

static res_T
load_mtl(struct rngrd* ground, const char* filename, struct mrumtl** out_mtl)
{
  struct mrumtl_create_args args = MRUMTL_CREATE_ARGS_DEFAULT;
  struct mrumtl* mtl = NULL;
  res_T res = RES_OK;
  ASSERT(ground && filename && out_mtl);

  args.verbose = ground->verbose;
  args.logger = ground->logger;
  args.allocator = ground->allocator;
  res = mrumtl_create(&args, &mtl);
  if(res != RES_OK) {
    log_err(ground, "%s: could not create the MruMtl data structure\n", filename);
    goto error;
  }

  res = mrumtl_load(mtl, filename);
  if(res != RES_OK) goto error;

exit:
  *out_mtl = mtl;
  return res;
error:
  if(mtl) { MRUMTL(ref_put(mtl)); mtl = NULL; }
  goto exit;
}

static res_T
load_mtllst(struct rngrd* ground, const struct rngrd_create_args* args)
{
  struct rnsl_create_args rnsl_args = RNSL_CREATE_ARGS_DEFAULT;
  struct rnsl* rnsl = NULL;
  size_t imtl, nmtls;
  res_T res = RES_OK;

  /* Create loader of material paths */
  rnsl_args.logger = ground->logger;
  rnsl_args.allocator = ground->allocator;
  rnsl_args.verbose = ground->verbose;
  res = rnsl_create(&rnsl_args, &rnsl);
  if(res != RES_OK) {
    log_err(ground, "Failed to create loader for material list `%s' -- %s\n",
      args->mtllst_filename, res_to_cstr(res));
    goto error;
  }

  /* Load the list of material paths */
  res = rnsl_load(rnsl, args->mtllst_filename);
  if(res != RES_OK) goto error;

  /* Reserve memory space for the list of materials */
  nmtls = rnsl_get_strings_count(rnsl);
  res = darray_mtl_resize(&ground->mtls, nmtls);
  if(res != RES_OK) {
    log_err(ground, "%s: could not allocate the list of %lu materials -- %s\n",
      args->mtllst_filename, nmtls, res_to_cstr(res));
    goto error;
  }

  /* Load the list of materials and create their associated BSDFs */
  FOR_EACH(imtl, 0, nmtls) {
    struct mtl* mtl = darray_mtl_data_get(&ground->mtls)+imtl;
    const char* filename = rnsl_get_string(rnsl, imtl);

    res = load_mtl(ground, filename, &mtl->mrumtl);
    if(res != RES_OK) goto error;
    res = mtl_create_bsdf_list(ground, filename, mtl);
    if(res != RES_OK) goto error;
  }

exit:
  if(rnsl) RNSL(ref_put(rnsl));
  return res;
error:
  darray_mtl_clear(&ground->mtls);
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rngrd_create_bsdf
  (struct rngrd* ground,
   const struct rngrd_create_bsdf_args* args,
   struct ssf_bsdf** out_bsdf)
{
  const struct ALIGN(8) { uint32_t mtl_id; float temperature; }* item = NULL;
  struct mtl* mtl = NULL;
  struct ssf_bsdf* bsdf = NULL;
  size_t ibsdf;
  struct sbuf_desc desc;
  res_T res = RES_OK;

  if(!ground || !out_bsdf) { res = RES_BAD_ARG; goto error; }
  res = check_create_bsdf_args(ground, args);
  if(res != RES_OK) goto error;

  /* Retrieve the material id of the primitive to consider */
  res = sbuf_get_desc(ground->props, &desc);
  if(res != RES_OK) goto error;
  item = sbuf_desc_at(&desc, args->prim.prim_id);

  /* Retrieve the spectrally varying material of the primitive */
  ASSERT(item->mtl_id < darray_mtl_size_get(&ground->mtls));
  mtl = darray_mtl_data_get(&ground->mtls)+item->mtl_id;

  /* Get the BSDF based on the input wavelength */
  res = mrumtl_fetch_brdf(mtl->mrumtl, args->wavelength, args->r, &ibsdf);
  if(res != RES_OK) goto error;
  bsdf = darray_bsdf_data_get(&mtl->bsdf_lst)[ibsdf];
  ASSERT(bsdf);
  SSF(bsdf_ref_get(bsdf));

exit:
  if(out_bsdf) *out_bsdf = bsdf;
  return res;

error:
  log_err(ground, "%s: error creating the BRDF for the primitive %lu "
    "at the wavelength %g -- %s\n",
    FUNC_NAME, (unsigned long)args->prim.prim_id, args->wavelength,
    res_to_cstr(res));
  if(bsdf) {
    SSF(bsdf_ref_put(bsdf));
    bsdf = NULL;
  }
  goto exit;
}

res_T
rngrd_get_temperature
  (const struct rngrd* ground,
   const struct rngrd_get_temperature_args* args,
   double* temperature)
{
  const struct ALIGN(8) { uint32_t mtl_id; float temperature; }* item = NULL;
  struct sbuf_desc desc;
  res_T res = RES_OK;

  if(!ground || !temperature) { res = RES_BAD_ARG; goto error; }
  res = check_get_temperature_args(ground, args);
  if(res != RES_OK) goto error;

  /* Retrieve the ground temperature */
  res = sbuf_get_desc(ground->props, &desc);
  if(res != RES_OK) goto error;
  item = sbuf_desc_at(&desc, args->prim.prim_id);
  ASSERT(item->temperature >= 0);

  *temperature = item->temperature;

exit:
  return res;
error:
  goto exit;
}

res_T
rngrd_get_temperature_range
  (const struct rngrd* ground,
   double T_range[2])
{
  struct sbuf_desc desc;
  size_t i;
  res_T res = RES_OK;

  if(!ground || !T_range) { res = RES_BAD_ARG; goto error; }

  res = sbuf_get_desc(ground->props, &desc);
  if(res != RES_OK) goto error;

  T_range[0] = +DBL_MAX;
  T_range[1] = -DBL_MAX;
  FOR_EACH(i, 0, desc.size) {
    const struct ALIGN(8) { uint32_t mtl_id; float temperature; }* item;
    item = sbuf_desc_at(&desc, i);
    ASSERT(item->temperature >= 0);

    T_range[0] = MMIN(T_range[0], item->temperature);
    T_range[1] = MMAX(T_range[1], item->temperature);
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
res_T
setup_properties(struct rngrd* ground, const struct rngrd_create_args* args)
{
  struct sbuf_create_args sbuf_args = SBUF_CREATE_ARGS_DEFAULT;
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;
  res_T res = RES_OK;
  ASSERT(ground && args);

  res = load_mtllst(ground, args);
  if(res != RES_OK) goto error;

  /* Create the Star-Buffer loader */
  sbuf_args.logger = ground->logger;
  sbuf_args.allocator = ground->allocator;
  sbuf_args.verbose = ground->verbose;
  res = sbuf_create(&sbuf_args, &ground->props);
  if(res != RES_OK) goto error;

  /* Load and retrieve properties */
  res = sbuf_load(ground->props, args->props_filename);
  if(res != RES_OK) goto error;
  res = sbuf_get_desc(ground->props, &sbuf_desc);
  if(res != RES_OK) goto error;
  res = check_sbuf_desc(ground, &sbuf_desc, args);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  if(ground->props) { SBUF(ref_put(ground->props)); ground->props = NULL; }
  darray_mtl_clear(&ground->mtls);
  goto exit;
}

res_T
check_properties(const struct rngrd* ground)
{
  struct sbuf_desc sbuf_desc = SBUF_DESC_NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(ground);

  /* The layout of sbuf_desc has already been checked when creating rngrd */
  res = sbuf_get_desc(ground->props, &sbuf_desc);
  if(res != RES_OK) goto error;

  FOR_EACH(i, 0, sbuf_desc.size) {
    const struct ALIGN(8) { uint32_t mtl_id; float temperature; }* item = NULL;
    item = sbuf_desc_at(&sbuf_desc, i);

    ASSERT(IS_ALIGNED(item, sbuf_desc.alitem));
    ASSERT(sizeof(*item) == sbuf_desc.szitem);

    if(item->mtl_id >= darray_mtl_size_get(&ground->mtls)) {
      log_err(ground,
        "%s: triangle %lu: invalid material id `%u'. It must be in [0, %lu[\n",
        str_cget(&ground->name), (unsigned long)i, item->mtl_id,
        (unsigned long)darray_mtl_size_get(&ground->mtls));
      res = RES_BAD_ARG;
      goto error;
    }

    if(item->temperature < 0) {
      log_err(ground, "%s: triangle %lu: invalid temperature `%g'\n",
        str_cget(&ground->name), i, item->temperature);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}
