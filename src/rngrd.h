/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNGRD_H
#define RNGRD_H

#include <star/s3d.h>

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(RNGRD_SHARED_BUILD) /* Build shared library */
  #define RNGRD_API extern EXPORT_SYM
#elif defined(RNGRD_STATIC) /* Use/build static library */
  #define RNGRD_API extern LOCAL_SYM
#else /* Use shared library */
  #define RNGRD_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the rngrd function `Func'
 * returns an error. One should use this macro on suvm function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define RNGRD(Func) ASSERT(rngrd_ ## Func == RES_OK)
#else
  #define RNGRD(Func) rngrd_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;
struct ssf_bsdf;

struct rngrd_create_args {
  const char* smsh_filename; /* The Star-Mesh geometry */
  const char* props_filename; /* Per triangle physical properties */
  const char* mtllst_filename; /* List of used materials */
  const char* name; /* Name of the ground */

  struct logger* logger; /* NULL <=> use default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define RNGRD_CREATE_ARGS_DEFAULT__ {                                          \
  NULL, /* Star-Mesh geometry */                                               \
  NULL, /* Per-triangle properties */                                          \
  NULL, /* List of used materials */                                           \
  "ground geometry", /* Name */                                                \
                                                                               \
  NULL, /* Logger */                                                           \
  NULL, /* Allocator */                                                        \
  0 /* Verbosity level */                                                      \
}
static const struct rngrd_create_args RNGRD_CREATE_ARGS_DEFAULT =
  RNGRD_CREATE_ARGS_DEFAULT__;

struct rngrd_trace_ray_args {
  double ray_org[3]; /* Ray origin */
  double ray_dir[3]; /* Ray direction */
  double ray_range[2]; /* Ray range */

  /* Intersection from which the ray starts. Used to avoid self intersection */
  struct s3d_hit hit_from;

  s3d_hit_filter_function_T filter; /* NULL <=> Stop RT at 1st hit triangle */
  void* filter_data; /* User data send to the filter function */
};
#define RNGRD_TRACE_RAY_ARGS_DEFAULT__ {                                       \
  {0,0,0}, /* Ray origin */                                                    \
  {0,0,1}, /* Ray direction */                                                 \
  {0,DBL_MAX}, /* Ray range */                                                 \
                                                                               \
  S3D_HIT_NULL__, /* Hit from */                                               \
                                                                               \
  NULL, /* Filter function */                                                  \
  NULL /* Filter data */                                                       \
}
static const struct rngrd_trace_ray_args RNGRD_TRACE_RAY_ARGS_DEFAULT =
  RNGRD_TRACE_RAY_ARGS_DEFAULT__;

struct rngrd_create_bsdf_args {
  struct s3d_primitive prim; /* Surface primitive to query */
  double barycentric_coords[3]; /* Position into and relative to `prim' */
  double wavelength; /* In nanometers */
  double r; /* Random number uniformly distributed in [0, 1[ */
};
#define RNGRD_CREATE_BSDF_ARGS_NULL__ {S3D_PRIMITIVE_NULL__, {0,0,0}, 0, 0}
static const struct rngrd_create_bsdf_args RNGRD_CREATE_BSDF_ARGS_NULL =
  RNGRD_CREATE_BSDF_ARGS_NULL__;

struct rngrd_get_temperature_args {
  struct s3d_primitive prim; /* Surface primitive to query */
  double barycentric_coords[3]; /* Position into and relative to `prim' */
};
#define RNGRD_GET_TEMPERATURE_ARGS_NULL__ {S3D_PRIMITIVE_NULL__, {0,0,0}}
static const struct rngrd_get_temperature_args RNGRD_GET_TEMPERATURE_ARGS_NULL =
  RNGRD_GET_TEMPERATURE_ARGS_NULL__;

/* Opaque data types */
struct rngrd;

BEGIN_DECLS

/*******************************************************************************
 * API of the Rad-Net GRounD library
 ******************************************************************************/
RNGRD_API res_T
rngrd_create
  (const struct rngrd_create_args* args,
   struct rngrd** ground);

RNGRD_API res_T
rngrd_ref_get
  (struct rngrd* ground);

RNGRD_API res_T
rngrd_ref_put
  (struct rngrd* ground);

/* Validates ground data. Data checks have already been done on load, but this
 * function performs longer tests: for example, it iterates over all surface
 * properties to check their validity against the mesh they are associated with
 * and the material list loaded */
RNGRD_API res_T
rngrd_validate
  (const struct rngrd* ground);

RNGRD_API res_T
rngrd_trace_ray
  (const struct rngrd* ground,
   struct rngrd_trace_ray_args* args,
   struct s3d_hit* hit);

RNGRD_API res_T
rngrd_create_bsdf
  (struct rngrd* ground,
   const struct rngrd_create_bsdf_args* args,
   struct ssf_bsdf** bsdf);

RNGRD_API res_T
rngrd_get_temperature
  (const struct rngrd* ground,
   const struct rngrd_get_temperature_args* args,
   double* temperature);

RNGRD_API res_T
rngrd_get_temperature_range
  (const struct rngrd* rngrd,
   double T_range[2]);

END_DECLS

#endif /* RNGRD_H */
