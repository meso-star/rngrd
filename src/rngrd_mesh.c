/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rngrd.h"
#include "rngrd_c.h"
#include "rngrd_log.h"

#include <star/s3d.h>
#include <star/smsh.h>

#include <rsys/cstr.h>
#include <rsys/float2.h>
#include <rsys/float3.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
hit_on_edge
  (const struct s3d_hit* hit,
   const float org[3],
   const float dir[3])
{
  const float on_edge_epsilon = 1.e-4f;

  struct s3d_attrib v0, v1, v2;
  float E0[3], E1[3], N[3];
  float tri_2area;
  float hit_2area0;
  float hit_2area1;
  float hit_2area2;
  float hit_pos[3];
  ASSERT(hit && !S3D_HIT_NONE(hit) && org && dir);

  /* Retrieve the triangle vertices */
  S3D(triangle_get_vertex_attrib(&hit->prim, 0, S3D_POSITION, &v0));
  S3D(triangle_get_vertex_attrib(&hit->prim, 1, S3D_POSITION, &v1));
  S3D(triangle_get_vertex_attrib(&hit->prim, 2, S3D_POSITION, &v2));

  /* Compute the intersection position */
  f3_add(hit_pos, org, f3_mulf(hit_pos, dir, hit->distance));

  /* Compute the area of the intersected triangle. Actually we compute the
   * area*2 to save computation time  */
  f3_sub(E0, v1.value, v0.value);
  f3_sub(E1, v2.value, v0.value);
  tri_2area = f3_len(f3_cross(N, E0, E1));

  /* Calculate the areas of the 3 triangles formed by an edge of the
   * intersecting triangle and the position of intersection.  Actually we
   * compute the areas*2 to save computation time. */
  f3_sub(E0, v0.value, hit_pos);
  f3_sub(E1, v1.value, hit_pos);
  hit_2area0 = f3_len(f3_cross(N, E0, E1));
  f3_sub(E0, v1.value, hit_pos);
  f3_sub(E1, v2.value, hit_pos);
  hit_2area1 = f3_len(f3_cross(N, E0, E1));
  f3_sub(E0, v2.value, hit_pos);
  f3_sub(E1, v0.value, hit_pos);
  hit_2area2 = f3_len(f3_cross(N, E0, E1));

  if(hit_2area0/tri_2area < on_edge_epsilon
  || hit_2area1/tri_2area < on_edge_epsilon
  || hit_2area2/tri_2area < on_edge_epsilon)
    return 1;

  return 0;
}

/* Returns 1 if the intersection found is a self-intersection, i.e. if the
 * intersection triangle is the triangle from which the ray starts. */
static INLINE int
self_hit
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   const float ray_range[2],
   const struct s3d_hit* hit_from)
{
  ASSERT(hit && hit_from);
  (void)ray_org, (void)ray_dir;

  if(S3D_HIT_NONE(hit_from))
    return 0;

  /* The intersected triangle is the one from which the ray starts. We ignore
   * this intersection */
  if(S3D_PRIMITIVE_EQ(&hit->prim, &hit_from->prim))
    return 1;

  /* If the intersection is close to the origin of the ray, we check if it is
   * on an edge/vertex shared by the triangle from which the ray originates. If
   * yes, we assume self-intersection and ignore it. */
  if(hit->distance/ray_range[1] < 1.e-4f
  && hit_on_edge(hit_from, ray_org, ray_dir)
  && hit_on_edge(hit, ray_org, ray_dir)) {
    return 1;
  }

  /* No self hit */
  return 0;
}

static int
mesh_filter
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   const float ray_range[2],
   void* ray_data,
   void* filter_data)
{
  const struct rngrd_trace_ray_args* ray_args = ray_data;
  (void)filter_data;

  /* Internally, Star-3D relies on Embree which, due to numerical imprecision,
   * can find intersections whose distances are not strictly within the ray
   * range. We reject these intersections. */
  if(hit->distance <= ray_range[0] || hit->distance >= ray_range[1])
    return 1;

  if(!ray_args) /* Nothing more to do */
    return 0;

  /* Discard this intersection */
  if(self_hit(hit, ray_org, ray_dir, ray_range, &ray_args->hit_from))
    return 1;

  if(!ray_args->filter) /* No user-defined filter functions */
    return 0;

  return ray_args->filter /* Invoke user-defined filtering */
    (hit, ray_org, ray_dir, ray_range, ray_args->filter_data, filter_data);
}

static void
get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  struct smsh_desc* desc = ctx;
  const uint64_t* indices = NULL;
  ASSERT(itri < desc->ncells && desc->dcell == 3);
  indices = smsh_desc_get_cell(desc, itri);
  ids[0] = (unsigned)indices[0];
  ids[1] = (unsigned)indices[1];
  ids[2] = (unsigned)indices[2];
}

static void
get_position(const unsigned ivert, float pos[3], void* ctx)
{
  struct smsh_desc* desc = ctx;
  const double* position = NULL;
  ASSERT(ivert < desc->nnodes && desc->dnode == 3);
  position = smsh_desc_get_node(desc, ivert);
  pos[0] = (float)position[0];
  pos[1] = (float)position[1];
  pos[2] = (float)position[2];
}

static res_T
check_smsh_desc(const struct rngrd* ground, const struct smsh_desc* desc)
{
  res_T res = RES_OK;
  ASSERT(ground && desc);

  if(desc->dnode != 3 && desc->dcell != 3) {
    log_err(ground,
      "The ground mesh must be a 3D triangular mesh "
      "(dimension of the mesh: %u; dimension of the vertices: %u)\n",
      desc->dnode, desc->dcell);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check number of triangles */
  if(desc->ncells > UINT_MAX) {
    log_err(ground,
      "The number of triangles cannot exceed %lu whereas it is %lu\n",
      (unsigned long)UINT_MAX, (unsigned long)desc->ncells);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check number of vertices */
  if(desc->nnodes > UINT_MAX) {
    log_err(ground,
      "The number of veritces cannot exceed %lu whereas it is %lu\n",
      (unsigned long)UINT_MAX, (unsigned long)desc->nnodes);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
setup_s3d(struct rngrd* ground, struct smsh_desc* smsh_desc)
{
  struct s3d_vertex_data vdata;
  struct s3d_shape* mesh = NULL;
  struct s3d_scene* scene = NULL;
  res_T res = RES_OK;

  res = s3d_device_create
    (ground->logger, ground->allocator, 0/*Make Star3D quiet*/, &ground->s3d);
  if(res != RES_OK) goto error;

  res = s3d_shape_create_mesh(ground->s3d, &mesh);
  if(res != RES_OK) goto error;
  res = s3d_mesh_set_hit_filter_function(mesh, mesh_filter, NULL);
  if(res != RES_OK) goto error;
  res = s3d_scene_create(ground->s3d, &scene);
  if(res != RES_OK) goto error;
  res = s3d_scene_attach_shape(scene, mesh);
  if(res != RES_OK) goto error;

  vdata.usage = S3D_POSITION;
  vdata.type = S3D_FLOAT3;
  vdata.get = get_position;
  res = s3d_mesh_setup_indexed_vertices(mesh, (unsigned)smsh_desc->ncells,
    get_indices, (unsigned)smsh_desc->nnodes, &vdata, 1, smsh_desc);
  if(res != RES_OK) goto error;

  res = s3d_scene_view_create(scene, S3D_TRACE, &ground->s3d_view);
  if(res != RES_OK) goto error;

exit:
  if(mesh) S3D(shape_ref_put(mesh));
  if(scene) S3D(scene_ref_put(scene));
  return res;
error:
  log_err(ground, "Could not setup the Star-3D data structures -- %s\n",
    res_to_cstr(res));
  if(ground->s3d) S3D(device_ref_put(ground->s3d));
  if(ground->s3d_view) S3D(scene_view_ref_put(ground->s3d_view));
  ground->s3d = NULL;
  ground->s3d_view = NULL;
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rngrd_trace_ray
  (const struct rngrd* ground,
   struct rngrd_trace_ray_args* args,
   struct s3d_hit* hit)
{
  float org[3];
  float dir[3];
  float range[2];
  res_T res = RES_OK;

  if(!ground || !args || !hit) {
    res = RES_BAD_ARG;
    goto error;
  }

  f3_set_d3(org, args->ray_org);
  f3_set_d3(dir, args->ray_dir);
  f2_set_d2(range, args->ray_range);

  *hit = S3D_HIT_NULL;

  res = s3d_scene_view_trace_ray(ground->s3d_view, org, dir, range, args, hit);
  if(res != RES_OK) {
    log_err(ground,
      "%s: error tracing ray "
      "(origin = %g, %g, %g; direction = %g, %g ,%g; range = %g, %g)\n",
      FUNC_NAME, SPLIT3(org), SPLIT3(dir), SPLIT2(range));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
res_T
setup_mesh(struct rngrd* ground, const struct rngrd_create_args* args)
{
  struct smsh_create_args smsh_args = SMSH_CREATE_ARGS_DEFAULT;
  struct smsh_desc smsh_desc = SMSH_DESC_NULL;
  struct smsh_load_args smsh_load_args = SMSH_LOAD_ARGS_NULL;
  struct smsh* smsh = NULL;
  res_T res = RES_OK;
  ASSERT(ground && args);

  /* Create the Star-Mesh loader */
  smsh_args.logger = ground->logger;
  smsh_args.allocator = ground->allocator;
  smsh_args.verbose = ground->verbose;
  res = smsh_create(&smsh_args, &smsh);
  if(res != RES_OK) goto error;

  /* Load and retrieve the Star-Mesh data */
  smsh_load_args.path = args->smsh_filename;
  smsh_load_args.memory_mapping = 1;
  res = smsh_load(smsh, &smsh_load_args);
  if(res != RES_OK) goto error;
  res = smsh_get_desc(smsh, &smsh_desc);
  if(res != RES_OK) goto error;
  res = check_smsh_desc(ground, &smsh_desc);
  if(res != RES_OK) goto error;

  /* Setup the Star-3D data structures */
  res = setup_s3d(ground, &smsh_desc);
  if(res != RES_OK) goto error;

  ground->ntriangles = smsh_desc.ncells;

exit:
  if(smsh) SMSH(ref_put(smsh));
  return res;
error:
  if(ground->s3d) {
    S3D(device_ref_put(ground->s3d));
    ground->s3d = NULL;
  }
  goto exit;
}
