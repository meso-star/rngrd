/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rngrd.h"

#include <rsys/mem_allocator.h>

#include <getopt.h>

struct args {
  struct rngrd_create_args rngrd;
  int validate;
  int quit;
};
#define ARGS_DEFAULT__ {RNGRD_CREATE_ARGS_DEFAULT__, 0, 0}
static const struct args ARGS_DEFAULT = ARGS_DEFAULT__;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  printf(
"Usage: %s -m mesh -p pops -M matlst [ option ... ]\n"
"Test the Rad-Net GRound library\n\n", cmd);
  printf(
"  -h             display this help and exit\n");
  printf(
"  -M matlst      path toward the list of ground materials (rnsl(5))\n");
  printf(
"  -m mesh        path toward the ground mesh (smsh(5))\n");
  printf(
"  -n name        ground name\n");
  printf(
"  -p props       path toward the ground properties (rnsp(5))\n");
  printf(
"  -V             validate data\n");
  printf(
"  -v             make the program verbose\n");
  printf("\n");
  printf(
"This is free software released under the GNU GPL license, version 3 or\n"
"later. You are free to change or redistribute it under certain\n"
"conditions <http://gnu.org.licenses/gpl.html>\n");
}

static void
args_release(struct args* args)
{
  ASSERT(args);
  *args = ARGS_DEFAULT;
}

static res_T
args_init(struct args* args, int argc, char** argv)
{
  res_T res = RES_OK;
  int opt;
  ASSERT(args && argc && argv);

  *args = ARGS_DEFAULT;

  while((opt = getopt(argc, argv, "hM:m:n:p:Vv")) != -1) {
    switch(opt) {
      case 'h':
        print_help(argv[0]);
        args_release(args);
        args->quit = 1;
        goto exit;
      case 'M': args->rngrd.mtllst_filename = optarg; break;
      case 'm': args->rngrd.smsh_filename = optarg; break;
      case 'n': args->rngrd.name = optarg; break;
      case 'p': args->rngrd.props_filename = optarg; break;
      case 'V': args->validate = 1; break;
      case 'v': args->rngrd.verbose = 1; break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option args `%s' -- `%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }
  if(!args->rngrd.smsh_filename) {
    fprintf(stderr, "Mesh filename is missing -- option `-m'\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!args->rngrd.props_filename) {
    fprintf(stderr, "Properties filename is missing -- option `-p'\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!args->rngrd.mtllst_filename) {
    fprintf(stderr, "The material list  filename is missing -- option `-M'\n");
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  args_release(args);
  goto exit;
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct args args = ARGS_DEFAULT;
  struct rngrd* rngrd = NULL;
  res_T res = RES_OK;
  int err = 0;

  res = args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(args.quit) goto exit;

  res = rngrd_create(&args.rngrd, &rngrd);
  if(res != RES_OK) goto error;

  if(args.validate) {
    res = rngrd_validate(rngrd);
    if(res != RES_OK) goto error;
  }

exit:
  args_release(&args);
  if(rngrd) RNGRD(ref_put(rngrd));
  if(mem_allocated_size() !=0) {
    fprintf(stderr, "Memory leaks: %lu bytes\n",
      (unsigned long)mem_allocated_size());
    err = -1;
  }
  return err;
error:
  err = -1;
  goto exit;
}
